import java.io.Serializable;
import java.time.YearMonth;
import java.util.Collection;


public class AR extends MonthlySelection implements Serializable {
 /**
	 * 
	 */
	private static final long serialVersionUID = 3220966609034393075L;
YearMonth ms;
	public AR() {
		super();
		super.mst = MonthlySelectionType.AR;
		this.ms = super.getSelection();
	}
	
	@Override
	Wine addWine(Wine w) {
		if(ms==YearMonth.now()){
			if(w.getVariety().equals("RED")){
				return w;
			}	
		}
		return null;
	}

}
