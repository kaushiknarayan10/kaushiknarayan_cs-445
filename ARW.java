import java.io.Serializable;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Collection;


public class ARW extends MonthlySelection implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7721272081559912234L;
	YearMonth ms;
	public ARW() {
		super();
		this.ms = super.getSelection();
		//super.mst = MonthlySelectionType.AR;
	}
	
	@Override
	Wine addWine(Wine w) {
		if(ms==YearMonth.now()){
			if(w.getVariety().toString().equals("RED") || w.getVariety().toString().equals("WHITE")){
				return w;
			}
		}
		return null;
	}
}
