import java.io.Serializable;
import java.time.YearMonth;
import java.util.Collection;



public class AW extends MonthlySelection implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6501050313135004135L;
	YearMonth ms;
	public AW() {
		super();
		super.mst = MonthlySelectionType.AW;
		this.ms = super.getSelection();
	}
	
	@Override
	Wine addWine(Wine w) {
		if(ms==YearMonth.now()){
			if(w.getVariety().equals("WHITE")){
				return w;
			}
		}
		return null;
	}
}
