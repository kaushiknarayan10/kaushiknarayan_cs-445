

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class AddSubscriber extends AddSubscriberRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1313944996703215021L;
	private Address a;
	private Subscriber s;
	FileOutputStream fout = null;
	ObjectOutputStream oos = null;
	ArrayList<Subscriber> sublist = new ArrayList();
	Collection<Subscriber> slist = new HashSet<Subscriber>();
	public AddSubscriber(String street, String city, String state, String zip,
			String name, String email, String phone, String tw, String fb) {
		super(street, city, state, zip, name, email, phone, tw, fb);
	}
	public AddSubscriber(String street, String city, String state, String zip,
			String name, String email, String phone){
		super(street, city, state, zip, name, email, phone);
	}
	public AddSubscriber(String street, String city, String state, String zip,
			String name, String email, String phone,String fb){
		super(street, city, state, zip, name, email, phone,fb);
	}
	public AddSubscriber(Address a,String name, String email, String phone,String tw){
		super(a,name, email, phone,tw);
	}
	
	
	
	@Override
	public AddSubscriberResponse addAccount(Collection<Subscriber> subs) {
		a = new Address(this.street, this.city, this.state, this.zip);
		s = new Subscriber (this.name, this.email, this.phone, a, this.facebook, this.twitter);
		
		if(nameCheck(s)) {
			AddSubscriberResponse ar = new SubscriberResponse(1000, false, "Name must be provided");
			return ar;
		}
		if(badName(subs, s)){
			AddSubscriberResponse ar = new SubscriberResponse(1001, false, "Bad name");
			return ar;
		}
		if(noEmail(s)){
			AddSubscriberResponse ar = new SubscriberResponse(1002, false, "E-mail ID must be provided");
			return ar;
		}
		if(invalidEmail(s)){
			AddSubscriberResponse ar = new SubscriberResponse(1003, false, "Invalid email ID");
			return ar;
		}
		if(noCity(a)){
			AddSubscriberResponse ar = new SubscriberResponse(1006, false, "City must be provided");
			return ar;
		}
		if(noState(a)){
			AddSubscriberResponse ar = new SubscriberResponse(1008, false, "State must be provided");
			return ar;
		}
		if(invalidState(a)){
			AddSubscriberResponse ar = new SubscriberResponse(1005, false, "Bad Address. State does not exist");
			return ar;
		}
		
		if (addressInBannedState(a)) {
			AddSubscriberResponse ar = new SubscriberResponse(1009, false, "Cannot ship to this state");
			return ar;
		}
		if(noZip(a)){
			AddSubscriberResponse ar = new SubscriberResponse(1010, false, "ZIP code must be provided");
			return ar;
		}
		if(badZip(a)){
			AddSubscriberResponse ar = new SubscriberResponse(1011, false, "Invalid ZIP code");
			return ar;
		}
		if(noPhone(s)){
			AddSubscriberResponse ar = new SubscriberResponse(1012, false, "Phone number must be provided");
			return ar;
		}
		
		if (userHasAccount(subs, s)) {
			AddSubscriberResponse ar = new SubscriberResponse(1014, false, "User already has an account");
			return ar;
		} else {
			
			subs.add(s);
			AddSubscriberResponse ar = new SubscriberResponse(s.getID(), true, "Congratulations");
			return ar;
			
			
		}
	}
	
	private boolean userHasAccount(Collection<Subscriber> subs, Subscriber s) {
		for(Subscriber s1 : subs){
			if(s1.name.equalsIgnoreCase(s.name))
				return true;
		}
		return false;
	}
	
	private boolean addressInBannedState(Address a) {
		for (NoShipList nsl : NoShipList.values()) {
			if(nsl.name().toString().equalsIgnoreCase(a.state.toString())) {
				return true;
			}
		}
		
		return false;
	}
	private boolean nameCheck(Subscriber s){
			if(s.name.isEmpty()){
				return true;
		}
		return false;
	}
	private boolean badName(Collection<Subscriber> subs, Subscriber s){
		for(Subscriber s1 : subs){
			if(s1.name.equalsIgnoreCase(s.name)){
				return true;
			}
		}
		return false;
	}
	private boolean noEmail(Subscriber s){
		if(s.email.isEmpty()){
				return true;
			}
		return false;
	}
	private boolean invalidEmail(Subscriber s){
		if(s.email.contains("@")){
				return false;
			}
		if(s.email.endsWith("com")){
			return false;
		}
		return true;
	}
	private boolean noCity(Address a){
		if(a.city.isEmpty()){
		return true;
	}
		return false;
	}
	private boolean noState(Address a){
		if(a.state.isEmpty()){
		return true;
	}
		return false;
	}
	private boolean invalidState(Address a) {
		for (ShipList sl : ShipList.values()) {
			if(sl.name().toString().equalsIgnoreCase(a.state.toString())) {
				return false;
			}
		}
		return true;
	}
	private boolean noZip(Address a){
		if(a.zip.isEmpty()){
		return true;
	}
		return false;
	}
	private boolean badZip(Address a){
		if(a.zip.length()>5){
		return true;
	}
		return false;
	}
	private boolean noPhone(Subscriber s){
		if(s.phone.isEmpty()){
		return true;
	}
		return false;
	}
}

