

import java.io.Serializable;
import java.util.Collection;

// Boundary class for a request to add a subscriber
public abstract class AddSubscriberRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 9219839769906543305L;
	String street, city, state, zip;
	String name, email, phone, twitter, facebook;
	
	
	public AddSubscriberRequest(String street, String c, String state, String z, String n, String e, String p, String tw, String fb) {
		this.street = street;
		this.city = c;
		this.state = state;
		this.zip = z;
		this.name = n;
		this.email = e;
		this.phone = p;
		this.twitter = tw;
		this.facebook = fb;
	}
	
	public AddSubscriberRequest(String street, String c, String state, String z, String n, String e, String p){
		this.street = street;
		this.city = c;
		this.state = state;
		this.zip = z;
		this.name = n;
		this.email = e;
		this.phone = p;
	}
	
	public AddSubscriberRequest(String street, String c, String state, String z, String n, String e, String p, String fb){
		this.street = street;
		this.city = c;
		this.state = state;
		this.zip = z;
		this.name = n;
		this.email = e;
		this.phone = p;
		this.facebook = fb;
	}
	
	public AddSubscriberRequest(Address a, String n, String e, String p, String tw){
		this.street = a.street;
		this.city = a.city;
		this.state = a.state;
		this.zip = a.zip;
		this.name = n;
		this.email = e;
		this.phone = p;
		this.twitter= tw;
	}
	public abstract AddSubscriberResponse addAccount(Collection<Subscriber> subs);
	
}
