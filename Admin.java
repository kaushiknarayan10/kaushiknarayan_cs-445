import java.util.Date;


public class Admin {
	public int ID;
	public String adminName;
	public Date createdDate;
	public int creatorID;
	public Admin(){
		this.ID = IdGenerator.newID();
		this.adminName = "Default Admin";
		this.createdDate = new Date();
	}
	public Admin(int ID, String name, Date date, int CID){
		this.ID = ID;
		this.adminName = name;
		this.createdDate = date;
		this.creatorID = CID;
	}
	int getID(){
		return this.ID;
	}
	
}
