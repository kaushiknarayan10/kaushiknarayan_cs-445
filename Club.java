import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;


public class Club implements Serializable{
/**
	 * 
	 */
	private static final long serialVersionUID = 6176307908264446111L;
	private String clubname;
	Collection<Subscriber> subs = new HashSet<Subscriber>();
	ArrayList<Wine> wines = new ArrayList<Wine>();
	public Club(){
		clubname = "VIN";
	}
	public Club(String name){
		this.clubname = name;
	}	
}
