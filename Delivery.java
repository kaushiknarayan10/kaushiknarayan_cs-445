import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

public class Delivery {
	public String partner;
	public Address partnerAddress;
	public Date deliveryDate;
	public double deliveryCharge = 12.75;
	public int deliveryID;
	public Delivery(){
		this.partner = "John Doe";
		this.partnerAddress = new Address();
		this.deliveryDate = new Date();
	}
	
	public Delivery(String name, Address a, Date d){
		this.partner = name;
		this.partnerAddress = a;
		this.deliveryDate = d;
	}

	public static void main(String[] args) {

		if(args.length==0){
			System.out.println(new DeliveryResponse(0,false,"Invalide number of arguments"));
		}
		else if(args.length==1){
			view(Integer.parseInt(args[0]));
		}
		else if(args.length==2){
			modify(Integer.parseInt(args[0]), args[1]);
		}
		else if (args.length==3){
			modify(Integer.parseInt(args[0]),args[1], args[2]);
		}
		else if(args.length==7){
			add(Integer.parseInt(args[0]),args[1], args[2], args[3], args[4],args[5], args[6]);
		}
		
		
		
	}
	
	public static void view(int ID){
		Collection<Subscriber>sublist= new HashSet<Subscriber>();
		 Club c = new Club("VIN");
			try {
				c = Main.restoreClubState(c);
				sublist = c.subs;
			} catch (IOException e) {
				System.err.println("IOException caught when trying to restore state");
			}
			for(Subscriber s1 : sublist){
				if(s1.ID==ID){
					try {
				 		c.subs = sublist;
						Main.saveClubState(c);
					} catch (IOException e) {
						System.err.println("IOException caught when trying to save state.\n");
					}
					System.out.println("Delivery Partner : "+s1.delivery.partner);
					System.out.println("Delivery Address : "+s1.delivery.partnerAddress);
				}
				else{
					System.out.println("Subscriber does not exist");
					
				}
			}
			try {
		 		c.subs = sublist;
				Main.saveClubState(c);
			} catch (IOException e) {
				System.err.println("IOException caught when trying to save state.\n");
			}
			System.out.println("Delivery partner not specified");
	}
	
	public static WineResponse add(int UID, String pname, String city, String street, String state, String zip, String date){
		Collection<Subscriber>sublist= new HashSet<Subscriber>();
		Address paddress = new Address(city, street, state, zip);
		SimpleDateFormat formatter = new SimpleDateFormat("DD-MON-YYYY");
		Date ddate = new Date();
		try {
			ddate = formatter.parse(date);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		 Club c = new Club("VIN");
			try {
				c = Main.restoreClubState(c);
				sublist = c.subs;
			} catch (IOException e) {
				System.err.println("IOException caught when trying to restore state");
			}
			
			for(Subscriber s1 : sublist){
				if(s1.ID==UID){
					s1.delivery.partner=pname;
					s1.delivery.partnerAddress=paddress;
					s1.delivery.deliveryDate = ddate;
					try {
				 		c.subs = sublist;
						Main.saveClubState(c);
					} catch (IOException e) {
						System.err.println("IOException caught when trying to save state.\n");
					}
					return new WineResponse(UID, true, "Partner details added");
				}
				return new WineResponse(0, false, "Subscriber does not exist");
			}
			try {
	 		c.subs = sublist;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
			return new WineResponse(UID, false, "Subscriber not found");
			
	}
	
public static DeliveryResponse modify(int UID, String pname){
	Collection<Subscriber>sublist= new HashSet<Subscriber>();
	 Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		
		for(Subscriber s1 : sublist){
			if(s1.ID==UID){
				s1.delivery.partner.equals(pname);
				
				try {
			 		c.subs = sublist;
					Main.saveClubState(c);
				} catch (IOException e) {
					System.err.println("IOException caught when trying to save state.\n");
				}
				return new DeliveryResponse(UID, true, "Modified the Partner");
			}
		}
		try {
 		c.subs = sublist;
		Main.saveClubState(c);
	} catch (IOException e) {
		System.err.println("IOException caught when trying to save state.\n");
	}
		return new DeliveryResponse(0, false, "Subscriber does not exist");
		
}

public static DeliveryResponse modify(int UID, String pname, String newdate){
	Collection<Subscriber>sublist= new HashSet<Subscriber>();
	SimpleDateFormat formatter = new SimpleDateFormat("DD-MON-YYYY");
	Date ddate = new Date();
	try {
		ddate = formatter.parse(newdate);
	} catch (ParseException e1) {
		e1.printStackTrace();
	}
	 Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		
		for(Subscriber s1 : sublist){
			if(s1.ID==UID){
				s1.delivery.partner.equals(pname);
				s1.delivery.deliveryDate.equals(ddate);
				try {
			 		c.subs = sublist;
					Main.saveClubState(c);
				} catch (IOException e) {
					System.err.println("IOException caught when trying to save state.\n");
				}
				return new DeliveryResponse(UID, true, "Modified the Partner and Delivery Date");
			}
		}
		try {
	 		c.subs = sublist;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		return new DeliveryResponse(0, false, "Subscriber does not exist");
 }
}
