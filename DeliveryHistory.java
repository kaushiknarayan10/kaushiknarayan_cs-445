import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;


public class DeliveryHistory {
	public int deliveryID;
	public String deliveryStatus;
	public Date deliveryDate;
	ArrayList<Wine> wlist = new ArrayList<Wine>();
	static ArrayList<DeliveryHistory> dh = new ArrayList<DeliveryHistory>();
	public int SID;
	
	DeliveryHistory(String status, Date ddate, ArrayList<Wine> winelist, int SID){
		this.deliveryStatus = status;
		this.deliveryDate = ddate;
		this.wlist = winelist;
		this.deliveryID = IdGenerator.newID();
		this.SID = SID;
	}
	
	public void add(Collection<Subscriber> subs){
		for(Subscriber s1 : subs){
			dh.add(new DeliveryHistory(this.deliveryStatus, this.deliveryDate, this.wlist, s1.ID));
		}
	}
	
	public static void main(String args[]){
		view(Integer.parseInt(args[0]));
	}
	
	@SuppressWarnings("static-access")
	public static void view(int UID){
		Collection<Subscriber> sublist = new HashSet<Subscriber>();
		Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist=c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		
		for(Subscriber s1 : sublist){
			if(s1.ID==UID){
				for(DeliveryHistory d1 : dh){
					if(d1.SID==UID){
						System.out.println("History :");
						System.out.println("UID : "+UID);
						System.out.println("Shipments : ");
						for(Shipments sh : s1.shipments.shiplist){
							System.out.println(sh.view(UID).toString());
						}
						System.out.println("Wines : ");
						for(Wine w1 : s1.winelist){
							System.out.println("Wine ID : "+w1.getID());
							System.out.println("Label Name : "+w1.getLabelName());
						}
					}
					else{
						System.out.println("No Delivery History for the Subscriber");
					}
				}
			}
			else {
				System.out.println("Subscriber does not exist");
			}
		}
		
		
	}
}
//create deliveryhistiry constructor and add subs in that