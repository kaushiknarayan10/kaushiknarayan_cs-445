import java.io.Serializable;


public class DeliveryResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4586711068315264309L;
	private int ID;
	private boolean status;
	private String failureDescription;
	
	public DeliveryResponse(int id, boolean s, String f) {
		this.ID = id;
		this.status = s;
		this.failureDescription = f;
	}
}
