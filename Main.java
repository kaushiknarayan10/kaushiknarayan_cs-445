import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.io.*;

public class Main {
	
	public static void saveClubState(Club c) throws IOException {
		FileOutputStream fout = null;
		ObjectOutputStream oos = null;
		try {
			fout = new FileOutputStream("C:/Kaushik/Sem - 1/CS-445-OODP/Project/Source/kaushik-narayan.ser", true);
			oos = new ObjectOutputStream(fout);
			oos.writeObject(c);
			fout = new FileOutputStream("C:/Kaushik/Sem - 1/CS-445-OODP/Project/Source/Subscriber.ser", true);
			oos = new ObjectOutputStream(fout);
			oos.writeObject(c.subs);
		} catch (IOException e) {
		        e.printStackTrace();
		} finally {
			if(oos != null) {
				oos.close();
			} 
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Club restoreClubState(Club c) throws IOException {
		ObjectInputStream ois = null;
		try {
			FileInputStream fis = new FileInputStream("C:/Kaushik/Sem - 1/CS-445-OODP/Project/Source/kaushik-narayan.ser");
			ois = new ObjectInputStream(fis);
			c = (Club)ois.readObject();
			ois = null;
			FileInputStream fis1 = new FileInputStream("C:/Kaushik/Sem - 1/CS-445-OODP/Project/Source/Subscriber.ser");
			ois = new ObjectInputStream(fis1);
			c.subs= (Collection<Subscriber>)ois.readObject();
			return c;
		} catch (IOException e) {
			System.err.println("Nothing to restore.\n");
			System.out.println(e.getMessage());
			System.out.println(e.getClass().toString());
			e.printStackTrace();
		} 
		catch (ClassNotFoundException e) {
			System.err.println("ClassNotFoundException caught in restoreClubState()");
			e.printStackTrace();
		}
			finally {
			if (ois != null) {
				ois.close();
			} 
		}
		return c;
	}
	
	public static void main(String[] args) {
		Collection<Subscriber> subs = new HashSet<Subscriber>();
		Club c = new Club("VIN");
		
		
		try {
			c = restoreClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		
		try {
			saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		
	}
}	

