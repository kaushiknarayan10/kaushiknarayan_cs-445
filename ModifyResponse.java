import java.io.Serializable;


public class ModifyResponse extends ModifySubscriberResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8120355860207771861L;

	public ModifyResponse(int id, boolean s, String f) {
		super(id, s, f);
	}
	
}
