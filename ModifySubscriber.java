import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;


public class ModifySubscriber extends ModifySubscriberRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1465104052150104398L;
	Collection<Subscriber> subs = new HashSet<Subscriber>();
	int modifier;

	public ModifySubscriber(int id, String name, String email, String phone, String tw, String fb) {
		super(id, name, email, phone, tw, fb);
	}
	public ModifySubscriber(int id, String name, String email, String phone, String tw) {
		super(id, name, email, phone, tw);
	}
	public ModifySubscriber(int id,String name, String email, String phone) {
		super(id, name, email, phone);
	}
	public ModifySubscriber(int id, String name, String email) {
		super(id, name, email);
	}
	
	public ModifySubscriber(int id, String name) {
		super(id, name);
	}
	public ModifySubscriber(int id) {
		super(id);
		this.modifier = 10;
	}
	
	
	@Override
	public ModifySubscriberResponse modifyAccount(int ID, Collection<Subscriber> subs) {
		for(Subscriber s : subs){
			if(s.ID == ID){
				if(nameCheck(s)) {
					ModifySubscriberResponse ms = new ModifyResponse(1000, false, "Name must be provided");
					return ms;
				}
				if(badName(subs, s)){
					ModifySubscriberResponse ms = new ModifyResponse(1001, false, "Bad name");
					return ms;
				}
				if(noEmail(s)){
					ModifySubscriberResponse ms = new ModifyResponse(1002, false, "E-mail ID must be provided");
					return ms;
				}
				if(invalidEmail(s)){
					ModifySubscriberResponse ms = new ModifyResponse(1003, false, "Invalid email ID");
					return ms;
				}
				if(noCity(s.address)){
					ModifySubscriberResponse ms = new ModifyResponse(1006, false, "City must be provided");
					return ms;
				}
				if(noState(s.address)){
					ModifySubscriberResponse ms = new ModifyResponse(1008, false, "State must be provided");
					return ms;
				}
				if(invalidState(s.address)){
					ModifySubscriberResponse ms = new ModifyResponse(1005, false, "Bad Address. State does not exist");
					return ms;
				}
				
				if (addressInBannedState(s.address)) {
					ModifySubscriberResponse ms = new ModifyResponse(1009, false, "Cannot ship to this state");
					return ms;
				}
				if(noZip(s.address)){
					ModifySubscriberResponse ms = new ModifyResponse(1010, false, "ZIP code must be provided");
					return ms;
				}
				if(badZip(s.address)){
					ModifySubscriberResponse ms = new ModifyResponse(1011, false, "Invalid ZIP code");
					return ms;
				}
				if(noPhone(s)){
					ModifySubscriberResponse ms = new ModifyResponse(1012, false, "Phone number must be provided");
					return ms;
				}
				
				else {
					s.name = this.name;
					s.email = this.email;
					s.phone = this.phone;
					s.facebook = this.facebook;
					s.twitter = this.twitter;
				}
			}
			else{
				ModifySubscriberResponse ms = new ModifyResponse(0, false, "No such User exists");
				return ms;
			}
		}
		ModifySubscriberResponse ms = new ModifyResponse(ID, true, "Successfully modified");
		return ms;
	}
	
	
	private boolean addressInBannedState(Address a) {
		for (NoShipList nsl : NoShipList.values()) {
			if(nsl.name().toString().equalsIgnoreCase(a.state.toString())) {
				return true;
			}
		}
		
		return false;
	}
	private boolean nameCheck(Subscriber s){
			if(s.name.isEmpty()){
				return true;
		}
		return false;
	}
	private boolean badName(Collection<Subscriber> subs, Subscriber s){
		for(Subscriber s1 : subs){
			if(s1.name.equalsIgnoreCase(s.name)){
				return true;
			}
		}
		return false;
	}
	private boolean noEmail(Subscriber s){
		if(s.email.isEmpty()){
				return true;
			}
		return false;
	}
	private boolean invalidEmail(Subscriber s){
		if(s.email.contains("@")){
				return false;
			}
		if(s.email.endsWith("com")){
			return false;
		}
		return true;
	}
	private boolean noCity(Address a){
		if(a.city.isEmpty()){
		return true;
	}
		return false;
	}
	private boolean noState(Address a){
		if(a.state.isEmpty()){
		return true;
	}
		return false;
	}
	private boolean invalidState(Address a) {
		for (ShipList sl : ShipList.values()) {
			if(sl.name().toString().equalsIgnoreCase(a.state.toString())) {
				return false;
			}
		}
		return true;
	}
	private boolean noZip(Address a){
		if(a.zip.isEmpty()){
		return true;
	}
		return false;
	}
	private boolean badZip(Address a){
		if(a.zip.length()>5){
		return true;
	}
		return false;
	}
	private boolean noPhone(Subscriber s){
		if(s.phone.isEmpty()){
		return true;
	}
		return false;
	}
}
