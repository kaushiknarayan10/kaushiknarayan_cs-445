import java.io.Serializable;
import java.util.Collection;


public abstract class ModifySubscriberRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2388229298082522020L;
	String name, email, phone, twitter, facebook;
	int ID;
	
	public ModifySubscriberRequest(int ID, String n, String e, String p, String tw, String fb) {
		this.ID = ID;
		this.name = n;
		this.email = e;
		this.phone = p;
		this.twitter = tw;
		this.facebook = fb;
	}
	public ModifySubscriberRequest(int ID,String n, String e, String p, String fb) {
		this.ID = ID;
		this.name = n;
		this.email = e;
		this.phone = p;
		this.facebook = fb;
	}
	public ModifySubscriberRequest(int ID, String n, String e, String p) {
		this.ID = ID;
		this.name = n;
		this.email = e;
		this.phone = p;
	}
	public ModifySubscriberRequest(int ID, String n, String e) {
		this.ID = ID;
		this.name = n;
		this.email = e;
	}
	
	public ModifySubscriberRequest(int ID, String n) {
		this.ID = ID;
		this.name = n;
	}
	public ModifySubscriberRequest(int ID) {
		this.ID = ID;
	}
	public abstract ModifySubscriberResponse modifyAccount(int ID, Collection<Subscriber> subs);
	
}

