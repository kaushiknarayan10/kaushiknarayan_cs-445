import java.io.Serializable;


public abstract class ModifySubscriberResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2152687796122008512L;
	private int ID;
	private boolean status;
	private String failureDescription;
	
	public ModifySubscriberResponse(int id, boolean s, String f) {
		this.ID = id;
		this.status = s;
		this.failureDescription = f;
	}
	
	public void printResponse()
	{
		System.out.println("ID: " + this.ID + "\tStatus: " + this.status + "\tDescription: " + this.failureDescription); 
		
	}
}
