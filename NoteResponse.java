
public class NoteResponse {
	private int ID;
	private boolean status;
	private String failureDescription;
	
	public NoteResponse(int id, boolean s, String f) {
		this.setID(id);
		this.setStatus(s);
		this.setFailureDescription(f);
	}

	public String getFailureDescription() {
		return failureDescription;
	}

	public void setFailureDescription(String failureDescription) {
		this.failureDescription = failureDescription;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}
}
