import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;


public class Notes {
	public int noteID;
	public String notes;
	public static Collection<Subscriber> subs = new HashSet<Subscriber>();
	static Notes n;
	public Notes(){
		this.notes = "Default note";
		this.noteID = IdGenerator.newID();
	}
	public Notes(String note){
		this.notes = note;
		this.noteID = IdGenerator.newID();
	}
	public int getNoteID(){
		return this.noteID;
	}
	public static void main(String[] args) {
		if(args.length==0){
			System.out.println(new NoteResponse(0,false,"No arguments given"));
		}
		else if(args.length==1){
			view(Integer.parseInt(args[0]));
		}
		else if(args.length==2){
			view(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
			add(Integer.parseInt(args[0]), args[1]);
			delete(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
			modify(Integer.parseInt(args[0]),args[1]);
			delete(Integer.parseInt(args[0]),args[1]);
		}
		else if(args.length==3){
			modify(Integer.parseInt(args[0]),args[1],Integer.parseInt(args[2]));
		}
		else{
			new NoteResponse(0, false, "Too many Arguments");
		}
	}
	public static void view(int ID){
		Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			subs = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
			}
		
		System.out.println("User ID : "+ID);
		for(Subscriber s1 : subs){
			if(s1.ID== ID){
				for(Wine w : s1.getWineList()){
					for(Notes n : w.notes){
						System.out.println("Note ID : "+n.noteID);
						System.out.println("Notes : "+n.notes);
					}
				}
			}
		}
	try {
		c.subs=subs;
		Main.saveClubState(c);
	} catch (IOException e) {
		System.err.println("IOException caught when trying to save state.\n");
	}
		
	}
	public static void view(int ID, int wineID){
		Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			subs = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
			}
		
		System.out.println("User ID : "+ID);
		for(Subscriber s1 : subs){
			if(s1.ID== ID){
				for(Wine w : s1.getWineList()){
					if(w.getID()==wineID){
					for(Notes n : w.notes){
						System.out.println("Note ID : "+n.noteID);
						System.out.println("Notes : "+n.notes);
					}
					}
				}
			}
		}
		
		try {
			c.subs = subs;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}	
		
	}
	
	public static NoteResponse add(int ID, String note){
		Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			//subs = Main.restoreSubscriber(subs);
			subs = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
			}
		
		for(Subscriber s1 : subs){
			if(s1.ID == ID){
				ArrayList<Wine> wlist = s1.getWineList();
				for(Wine w : wlist){
					w.notes.add(new Notes(note));
				}
				
			}
			else if(note.isEmpty()){
				return new NoteResponse(0, false, "Empty note");
			}
			else if(s1.ID != ID){
				return new NoteResponse(0, false, "No User exists");
			}
		}
	
		try {
			c.subs = subs;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		return new NoteResponse(1, true, "Note successfully added");
	}
	
	public static NoteResponse modify(int ID, String note){
		Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			//subs = Main.restoreSubscriber(subs);
			subs = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
			}
		
		for(Subscriber s1 : subs){
			if(s1.ID == ID){
				ArrayList<Wine> wlist = s1.getWineList();
				for(Wine w : wlist){
					w.notes.remove(w.notes.size()-1);
					w.notes.add(new Notes(note));
				}
			}
			else if(note.isEmpty()){
				return new NoteResponse(0, false, "Empty note");
			}
			else if(s1.ID != ID){
				return new NoteResponse(0, false, "No User exists");
			}
		}
		
		try {
			c.subs = subs;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		return new NoteResponse(1, true, "Note successfully modified");
	}
	
	public static NoteResponse modify(int ID, String note, int noteID){
		Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			//subs = Main.restoreSubscriber(subs);
			subs = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
			}
		for(Subscriber s1 : subs){
			if(s1.ID == ID){
				ArrayList<Wine> wlist = s1.getWineList();
				for(Wine w : wlist){
					for(Notes n1 : w.notes){
						if(n1.noteID== noteID){
							n1.notes=note;
						}
					}
					
				}
			}
			else if(note.isEmpty()){
				return new NoteResponse(0, false, "Empty note");
			}
			else if(s1.ID != ID){
				return new NoteResponse(0, false, "No User exists");
			}
		}
		
		try {
			c.subs = subs;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		
		return new NoteResponse(noteID, true, "Note successfully modified");
	}
	
	public static NoteResponse delete(int ID, String note){
		Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			//subs = Main.restoreSubscriber(subs);
			subs = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
			}
		int nid = 0;
		for(Subscriber s1 : subs){
			if(s1.ID == ID){
				ArrayList<Wine> wlist = s1.getWineList();
				for(Wine w : wlist){
						nid = n.noteID;
						w.notes.remove(note);
				}
			}
			else if(note.isEmpty()){
				return new NoteResponse(0, false, "Empty note");
			}
			else if(s1.ID != ID){
				return new NoteResponse(0, false, "No User exists");
			}
		}
		try {
			c.subs = subs;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		
		return new NoteResponse(nid, true, "Successfully deleted note");
	}
	
	public static NoteResponse delete(int ID,int noteID){
		Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			//subs = Main.restoreSubscriber(subs);
			subs = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
			}
		String note;
		for(Subscriber s1 : subs){
			if(s1.ID == ID){
				for(Wine w : s1.getWineList()){
					for(Notes n1 : w.notes){
						if(n1.noteID==noteID){
							note= n1.notes;
							w.notes.remove(note);
					}	
					else{
						return new NoteResponse(0, false, "No Note exists");
					}
				}
				}
			}
			else if(s1.ID != ID){
				return new NoteResponse(0, false, "No User exists");
			}
		}
		try {
			c.subs = subs;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		return new NoteResponse(noteID, true, "Note successfully modified");
	}
	
}
