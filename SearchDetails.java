import java.io.IOException;
import java.io.Serializable;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;


public class SearchDetails implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 692667383013697882L;
	public static ArrayList<Notes> notes = new ArrayList<Notes>();
	public static int shipmentID;
	public static ArrayList <Wine> wlist = new ArrayList<Wine>();
	public static ArrayList<Shipments> slist = new ArrayList<Shipments>();
	public static ArrayList<SearchDetails> sd = new ArrayList<SearchDetails>();
	public static YearMonth ms;
	SearchDetails(ArrayList<Wine> wines, ArrayList<Notes> notes, int ID, YearMonth ms){
		SearchDetails.wlist = wines;
		SearchDetails.notes = notes;
		SearchDetails.shipmentID = ID;
		SearchDetails.ms = ms;
		
	}
	SearchDetails(ArrayList<Wine> wines, ArrayList<Notes> notes, YearMonth ms, ArrayList<Shipments> shiplist){
		SearchDetails.wlist = wines;
		SearchDetails.notes = notes;
		SearchDetails.ms = ms;
		SearchDetails.slist = shiplist;
		
	}
	public  static ArrayList<SearchDetails> search(int ID, Collection<Subscriber> sublist){
		
		for(Subscriber s1 : sublist){
			if(s1.ID == ID){
			wlist = s1.getWineList();
			for(Wine w : wlist){
				notes.addAll(w.notes);
			}
			shipmentID = s1.shipments.shipmentID;
			ms = s1.ms;
			sd.add(new SearchDetails(wlist, notes, shipmentID, ms));
		}
		}	
		return sd;
		
	}
	
	public static ArrayList<SearchDetails> search(int ID, int SID, Collection<Subscriber>sublist){
		for(Subscriber s1 : sublist){
			if(s1.ID == ID){
				if(s1.shipments.shipmentID==SID){
					wlist = s1.getWineList();
					for(Wine w : wlist){
						notes.addAll(w.notes);
					}
					ms = s1.ms;
					sd.add(new SearchDetails(wlist, notes, ms, Shipments.view(ID)));
				}
			
				}
			}
		return sd;
		
	}
	
	
	public Collection<Wine> getWinelist(){
		return SearchDetails.wlist;
	}
	public int getShipmentID(){
		return SearchDetails.shipmentID;
	}
	
}
