
import java.io.IOException;
import java.io.Serializable;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;


public class Shipments implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2167380009914889876L;
	int shipmentID;
	YearMonth selectionMonth;
	String Status;
	ArrayList<Wine> wlist = new ArrayList<Wine>();
	double shipmentsPrice = 60;
	static ArrayList<Shipments> shiplist = new ArrayList<Shipments>();
	
	public Shipments(){
		this.shipmentID = IdGenerator.newID();
		this.selectionMonth = YearMonth.now();
		this.Status = "Pending";
		
	}
	public Shipments(int ID, YearMonth ym, String status){
		this.shipmentID = ID;
		this.selectionMonth = ym;
		this.Status = status;
	}
	public Shipments(int ID, YearMonth ym, String status, ArrayList<Wine> wlist){
		this.shipmentID = ID;
		this.selectionMonth = ym;
		this.Status = status;
		this.wlist = wlist;
		
	}
	
	public static void main(String args[]){
		
		if(args.length<1){
			System.out.println("No arguments passed");
		}
		else if(args.length == 1){
			view(Integer.parseInt(args[0]));
		}

	}
	public static ArrayList<Shipments> view(int ID){
		Club c = new Club("VIN");
		Collection<Subscriber> sublist = new HashSet<Subscriber>();
		try {
			c = Main.restoreClubState(c);
			sublist=c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		
			for(Subscriber s1 : sublist){
				if(s1.ID == ID){
				shiplist.add(new Shipments(s1.shipments.shipmentID, s1.ms, s1.shipments.Status));
				}
			}
			
			try {
				c.subs=sublist;
				 Main.saveClubState(c);
				
			} catch (IOException e) {
				System.err.println("IOException caught when trying to restore state");
			}
			return shiplist;
	}
	
	public static ArrayList<Shipments> view(int ID, int SID){
		Collection<Subscriber> sublist = new HashSet<Subscriber>();
		ArrayList<Wine> winelist = new ArrayList<Wine>();
		Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist=c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		
			for(Subscriber s1 : sublist){
				if(s1.ID == ID){
					if(s1.shipments.shipmentID==SID){
						winelist = s1.getWineList();
						shiplist.add(new Shipments(s1.shipments.shipmentID, s1.ms, s1.shipments.Status, winelist));
					}
				}
			}
			
			try {
				c.subs=sublist;
				 Main.saveClubState(c);
				
			} catch (IOException e) {
				System.err.println("IOException caught when trying to restore state");
			}
			return shiplist;
	}
	
	public static ShipmentsResponse add(int UID){
		Collection<Subscriber> sublist = new HashSet<Subscriber>();
		Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist=c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		
		for(Subscriber s1 : sublist){
			if(s1.ID==UID){
				if(s1.winelist.size()<6){
					return new ShipmentsResponse(0, false, "Less than 6 wines present. Add more wines");
				}
				else if(s1.delivery.partner.isEmpty()){
					return new ShipmentsResponse(0, false, "Please add a Delivery partner");
				}
				else if(s1.delivery.partnerAddress.toString().isEmpty()){
					return new ShipmentsResponse(0, false, "Please add a Delivery Address");
				}
				else if(s1.delivery.deliveryDate.toString().isEmpty()){
					return new ShipmentsResponse(0, false, "Please add a Delivery Date");
				}
				else if(s1.winelist.size()>6){
					if(s1.delivery.deliveryDate.compareTo(new Date()) > 0){
						s1.shipments.shipmentID = IdGenerator.newID();
						s1.shipments.Status = "Pending";
						for(int i=0; i<6; i++ ){
							for(Wine w1 : s1.getWineList()){
								s1.shipments.wlist.add(s1.winelist.get(i));
							}
						}
						s1.shipments.shipmentsPrice = s1.shipments.shipmentsPrice+s1.delivery.deliveryCharge;
						DeliveryHistory dw = new DeliveryHistory(s1.shipments.Status, s1.delivery.deliveryDate, s1.getWineList(), s1.ID);
						dw.add(sublist);
						try {
							c.subs=sublist;
							 Main.saveClubState(c);
							
						} catch (IOException e) {
							System.err.println("IOException caught when trying to restore state");
						}
						
						return new ShipmentsResponse(UID, true, "First 6 Wines added");
					}
					else if(s1.delivery.deliveryDate.compareTo(new Date()) == 0){
						s1.shipments.shipmentID = IdGenerator.newID();
						s1.shipments.Status = "Delivered";
						for(int i=0; i<6; i++ ){
							for(Wine w1 : s1.getWineList()){
								s1.shipments.wlist.add(s1.winelist.get(i));
							}
						}
						s1.shipments.shipmentsPrice = s1.shipments.shipmentsPrice+s1.delivery.deliveryCharge;
						DeliveryHistory dw = new DeliveryHistory(s1.shipments.Status, s1.delivery.deliveryDate, s1.getWineList(), s1.ID);
						dw.add(sublist);
						try {
							c.subs=sublist;
							 Main.saveClubState(c);
							
						} catch (IOException e) {
							System.err.println("IOException caught when trying to restore state");
						}
						
						return new ShipmentsResponse(UID, true, "First 6 Wines Delivered");
					}
					
				}
			}
			else{
				return new ShipmentsResponse(0, false, "Subscriber does not exist");
			}
		}
		try {
			c.subs=sublist;
			 Main.saveClubState(c);
			
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		return new ShipmentsResponse(0, false, "Subscriber does not exist");
	}
	
}
