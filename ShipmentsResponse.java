import java.io.Serializable;


public class ShipmentsResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7099073129349683324L;
	private int ID;
	private boolean status;
	private String failureDescription;
	
	public ShipmentsResponse(int id, boolean s, String f) {
		this.ID = id;
		this.status = s;
		this.failureDescription = f;
	}
}
