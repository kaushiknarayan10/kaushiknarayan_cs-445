import java.io.IOException;
import java.io.Serializable;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

public class Subscriber extends Club implements Serializable {
    
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5985017592368797146L;
	public String name, email, phone, twitter, facebook;
    public Address address;
    public MonthlySelectionType mst;
    public int ID;
    public ArrayList<Wine> winelist = new ArrayList<Wine>();
    public Date date;
    public Shipments shipments;
    public YearMonth ms;
    public Delivery delivery;
    //Wine w = new Wine();
    //init a wine type variable
    
    public Subscriber() {
    	this.name = "Jane Doe";
    	this.email = "jane.doe@example.com";
    	this.phone = "1234567890";
    	this.address = new Address();
    	this.mst = MonthlySelectionType.RW;
    	this.ID = IdGenerator.newID();
    	this.date = new Date();
    }
    public Subscriber (String name, String email, String phone, Address address) {
    	this.name = name;
    	this.email = email;
    	this.phone = phone.replaceAll("[\\s\\-()]", ""); // drop all non-digit characters
    	this.address = address;
    	this.mst = MonthlySelectionType.RW;
    	this.ID = IdGenerator.newID();
    	this.date = new Date();
    }
    public Subscriber (String name, String email, String phone, Address address, String fb, String tw) {
    	this.name = name;
    	this.email = email;
    	this.phone = phone.replaceAll("[\\s\\-()]", ""); // drop all non-digit characters
    	this.address = address;
    	this.twitter = tw;
    	this.facebook = fb;
    	this.mst = MonthlySelectionType.RW;
    	this.ID = IdGenerator.newID();
    	this.date = new Date();
    }

    private boolean isMatchName(String kw) {
    	String regex = "(?i).*" + kw + ".*";
    	return this.name.matches(regex);
    }

    private boolean isMatchEmail(String kw) {
    	String regex = "(?i).*" + kw + ".*";
    	return this.email.matches(regex);
    }

    private boolean isMatchPhone(String kw) {
    	String s = kw.replaceAll("[\\s\\-()]", ""); // drop all non-digit characters from search string
    	String regex = "(?i).*" + s + ".*";
    	return this.phone.matches(regex);
    }
    public boolean isMatch(String kw) {
    	if (isMatchName(kw) || isMatchEmail(kw) || isMatchPhone(kw)) {
    		return true;
    	} else return false;
    }

    public int getID() {
    	return this.ID;
    }

    public void updateInfo(String name, String email, String phone, Address address) {
    	this.name = name;
    	this.email = email;
    	this.phone = phone;
    	this.address = address;
    }
    
    public MonthlySelectionType getPreference() {
    	return mst;
    }
    
    public void setPreference(MonthlySelectionType t) {
    	this.mst = t;
    }
    
    public ArrayList<Wine> getWineList(){
    	return this.winelist;
    }
    public static void main(String args[]){
		if(args.length==0){
			add(0);
			System.out.println(add(0));
		}
		if(args.length==1){
			System.out.println(modify(0));
			System.out.println(search(Integer.parseInt(args[0])));
			view(Integer.parseInt(args[0]));
		}
		if(args.length==2){
			System.out.println(modify(Integer.parseInt(args[0]), args[1]));
			System.out.println(search(Integer.parseInt(args[0]), Integer.parseInt(args[1])));
		}
		if(args.length==3){
			modify(Integer.parseInt(args[0]), args[1],args[2]);
		}
		if(args.length==4){
			modify(Integer.parseInt(args[0]), args[1],args[2], args[3]);
		}
		if(args.length==5){
			modify(Integer.parseInt(args[0]), args[1],args[2], args[3], args[4]);
		}
		if(args.length==6){
			modify(Integer.parseInt(args[0]), args[1],args[2], args[3], args[4], args[5]);
		}
		
		if(args.length==7){
			add(args[0], args[1],args[2],args[3],args[4],args[5], args[6]);
			//System.out.println(add(args[0], args[1],args[2],args[3],args[4],args[5], args[6]));
		}
		if(args.length==8){
			if(args[7].startsWith("-f")){
				add(args[0], args[1],args[2],args[3],args[4],args[5],args[6],args[7]);
			}
			if(args[7].startsWith("-t")){
				Address a1 = new Address(args[2], args[3], args[4], args[5]);
				add(a1,args[0], args[1] ,args[6],args[7]);
			}
		}
		if(args.length==9){
			add(args[0], args[1],args[2],args[3],args[4],args[5],args[6],args[7], args[8]);
		}
		
    }
 public static AddSubscriberResponse add(int arg){
	 Collection<Subscriber>sublist= new HashSet<Subscriber>();
	 Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
	 	AddSubscriberResponse sr = new SubscriberResponse (0, false, "No arguments provided");
	 	
	 	try {
	 		c.subs = sublist;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		return sr;
 	}
 public static AddSubscriberResponse add(String name, String email, String street, String city, String state, String zip, String phone){
	 Collection<Subscriber>sublist= new HashSet<Subscriber>();
	 Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		
	 	AddSubscriberRequest a = new AddSubscriber(name, email, street, city, state, zip,phone);
		AddSubscriberResponse r = a.addAccount(sublist);
		
		try {
			c.subs = sublist;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		return r;
 	}
 public static AddSubscriberResponse add(String name, String email, String street, String city, String state, String zip, String phone, String fb){
	 Collection<Subscriber>sublist= new HashSet<Subscriber>();
	 Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
	 	AddSubscriberRequest a = new AddSubscriber(name, email, street, city, state, zip,phone, fb);
		AddSubscriberResponse r = a.addAccount(sublist);
		
		try {
			c.subs = sublist;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		return r;
 	}
 public static AddSubscriberResponse add(Address add, String name, String email, String phone, String tw){
	 Collection<Subscriber>sublist= new HashSet<Subscriber>();
	 Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
	 	AddSubscriberRequest a = new AddSubscriber(add,name, email,phone, tw);
		AddSubscriberResponse r = a.addAccount(sublist);
		
		try {
			c.subs = sublist;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		return r;
	}
 public static AddSubscriberResponse add(String name, String email, String street, String city, String state, String zip, String phone, String fb, String tw){
	 Collection<Subscriber>sublist= new HashSet<Subscriber>();
	 Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
	 	AddSubscriberRequest a = new AddSubscriber(name, email, street, city, state, zip,phone, fb, tw);
		AddSubscriberResponse r = a.addAccount(sublist);
		try {
			c.subs = sublist;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		return r;
	} 
 public static ModifySubscriberResponse modify(int arg){
	 	ModifySubscriberResponse sr = new ModifyResponse (0, false, "No arguments provided");
		return sr;
	}
 public static ModifySubscriberResponse modify(int ID, String name){
	 Collection<Subscriber>sublist= new HashSet<Subscriber>();
	 Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
	 	ModifySubscriberRequest msr = new ModifySubscriber(ID,name);
	 	ModifySubscriberResponse sr = msr.modifyAccount(ID, sublist);
	 	try {
	 		c.subs = sublist;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		return sr;
	}
 public static ModifySubscriberResponse modify(int ID, String name, String email){
	 Collection<Subscriber>sublist= new HashSet<Subscriber>();
	 Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
	 	ModifySubscriberRequest msr = new ModifySubscriber(ID,name,email);
	 	ModifySubscriberResponse sr = msr.modifyAccount(ID, sublist);
	 	try {
	 		c.subs = sublist;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		return sr;
	}
 
 
 
 public static ModifySubscriberResponse modify(int ID, String name, String email, String phone){
	 Collection<Subscriber>sublist= new HashSet<Subscriber>();
	 Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
	 	ModifySubscriberRequest msr = new ModifySubscriber(ID,name,email,phone);
	 	ModifySubscriberResponse sr = msr.modifyAccount(ID, sublist);
	 	try {
	 		c.subs = sublist;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		return sr;
	}
 public static ModifySubscriberResponse modify(int ID, String name, String email, String phone, String fb){
	 Collection<Subscriber>sublist= new HashSet<Subscriber>();
	 Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
	 	ModifySubscriberRequest msr = new ModifySubscriber(ID,name,email,phone,fb);
	 	ModifySubscriberResponse sr = msr.modifyAccount(ID, sublist);
	 	try {
	 		c.subs = sublist;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		return sr;
	}
 public static ModifySubscriberResponse modify(int ID, String name, String email, String phone, String fb, String tw){
	 Collection<Subscriber>sublist= new HashSet<Subscriber>();
	 Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
	 	ModifySubscriberRequest msr = new ModifySubscriber(ID,name,email,phone,fb,tw);
	 	ModifySubscriberResponse sr = msr.modifyAccount(ID, sublist);
	 	try {
	 		c.subs = sublist;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		return sr;
	}
 public static ArrayList<SearchDetails> search(int ID){
	 Collection<Subscriber>sublist= new HashSet<Subscriber>();
	 Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist = c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		ArrayList<SearchDetails> sd = SearchDetails.search(ID, sublist);
	 	try {
	 		c.subs = sublist;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
	 	return sd;
 }
 
 public static ArrayList<SearchDetails> search(int ID, int SID){
	 Collection<Subscriber>sublist= new HashSet<Subscriber>();
	 Club c = new Club("VIN");
	 
		try {
			c = Main.restoreClubState(c);
			sublist= c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		ArrayList<SearchDetails> sd = SearchDetails.search(ID, SID, sublist);
	 	
	 	try {
	 		c.subs = sublist;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
	 	return sd;
 }
 public static void view(int ID){
	 Collection<Subscriber>sublist= new HashSet<Subscriber>();
	 Club c = new Club("VIN");
	 
		try {
			c = Main.restoreClubState(c);
			sublist= c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		
		for(Subscriber s1 : sublist){
			if(s1.ID==ID){
				System.out.println("UID : "+ID);
				System.out.println("Date Created : "+s1.date);
				System.out.println("Type : "+s1.getPreference());
				System.out.println("Name : "+s1.name);
				System.out.println("Email : "+s1.email);
				System.out.println("Phone : "+s1.phone);
				System.out.println("Address");
				System.out.println("Street : "+s1.address.street);
				System.out.println("City : "+s1.address.city);
				System.out.println("State : "+s1.address.state);
				System.out.println("Zip : "+s1.address.zip);
				System.out.println("Facebook : "+s1.facebook);
				System.out.println("Twitter : "+s1.twitter);
			}
		}
		
		
		try {
	 		c.subs = sublist;
			Main.saveClubState(c);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
 }
}
