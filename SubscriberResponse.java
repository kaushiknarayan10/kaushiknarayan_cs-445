import java.io.Serializable;


public class SubscriberResponse extends AddSubscriberResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4880825154466040231L;

	public SubscriberResponse(int id, boolean s, String f) {
		super(id, s, f);
	}
	
}
