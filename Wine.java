

import java.io.IOException;
import java.io.Serializable;
import java.time.Year;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class Wine implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1032573553124927788L;
	public WineVariety wv;
	private WineType wt;
	private String labelName;
	private String grape;	// e.g. Merlot, Chardonnay, Riesling, etc.
	private String region;	// e.g. Napa, Russian Valley, etc.
	private String country; // e.g. France, USA, Australia, Chile
	private String maker;	// the wine maker, e.g. Sterling, Krupp Brother, etc.
	private Year year;		// Vintage year
	private static int numberOfRatings;
	private static float rating = 0;
	private int ID;
	public static ArrayList<Wine> wlist = new ArrayList<Wine>();
	public ArrayList<Notes> notes = new ArrayList<Notes>();

	public Wine() {
		this.wv = WineVariety.RED;
		this.wt = WineType.TABLE;
		this.labelName = "The Mission";
		this.grape = "Cabernet Sauvignon";
		this.region = "Napa";
		this.country = "USA";
		this.maker = "Sterling";
		this.year = Year.parse("2011");
		this.ID = IdGenerator.newID();
		this.notes.add(new Notes());
	}

	public Wine(WineVariety v, WineType t, String ln, String g, String r, String c, String m, Year y) {
		this.wv = v;
		this.wt = t;
		this.labelName = ln;
		this.grape = g;
		this.region = r;
		this.country = c;
		this.maker = m;
		this.year = y;
		this.ID = IdGenerator.newID();
		this.notes.add(new Notes());
	}

	public WineVariety getVariety() {
		return this.wv;
	}
	
	public WineType getType() {
		return this.wt;
	}

	public String getLabelName() {
		return this.labelName;
	}

	public String getGrape() {
		return this.grape;
	}

	public String getRegion() {
		return this.region;
	}
	
	public String getCountry() {
		return this.country;
	}

	public String getMaker() {
		return this.maker;
	}
	
	public String getYear() {
		return this.year.toString();
	}

	public int getNumberOfRatings() {
		return this.numberOfRatings;
	}
	
	public float getRating() {
		return this.rating;
	}

	public void addRating(int r) {
		numberOfRatings = numberOfRatings + 1;
		rating = rating*((float)(numberOfRatings - 1)/numberOfRatings) + (float)r/numberOfRatings;
	}
	public int getID(){
		return this.ID;
	}
	
	public boolean isMatch(String kw) {
        if (isMatchVariety(kw) || isMatchType(kw) || isMatchLabel(kw) || isMatchGrape(kw) || isMatchRegion(kw) || isMatchCountry(kw) || isMatchMaker(kw) || isMatchYear(kw)) {
                return true;
        } else return false;
	}
	    
    private boolean isMatchVariety(String kw) {
    	String regex = "(?i).*" + kw + ".*";
        return this.wv.name().matches(regex);
    }

    private boolean isMatchType(String kw) {
    	String regex = "(?i).*" + kw + ".*";
        return this.wt.name().matches(regex);
    }
    
    private boolean isMatchLabel(String kw) {
        String regex = "(?i).*" + kw + ".*";
        return this.labelName.matches(regex);
    }
    
    private boolean isMatchGrape(String kw) {
    	return false;
    }
    
    private boolean isMatchRegion(String kw) {
    	return false;
    }

    private boolean isMatchCountry(String kw) {
    	return false;
    }

    private boolean isMatchMaker(String kw) {
    	return false;
    }

    private boolean isMatchYear(String kw) {
    	return false;
    }
    
    public static void main(String args[]){
   
		if(args.length==0){
			view();
		}
		else if(args.length == 1){
			view(Integer.parseInt(args[0]));
		}
    	
    }
    
    public static ArrayList<Wine>view(){
    	Collection<Subscriber> sublist = new HashSet<Subscriber>();
    	Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist=c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		
    	for(Subscriber s : sublist){
    			wlist = s.winelist;
    		}
    	
    	try {
			c.subs=sublist;
			 Main.saveClubState(c);
			
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
    	return wlist;
    }
    
    public static ArrayList<Wine>view(int ID){
    	Collection<Subscriber> sublist = new HashSet<Subscriber>();
    	Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist=c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		
    	for(Subscriber s : sublist){
    		for(Wine w : s.winelist){
    			if(w.ID == ID){
    				wlist = s.winelist;
    			}
    		}
    	}
    	try {
			c.subs=sublist;
			 Main.saveClubState(c);
			
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
    	return wlist;
    }
    
    public static WineResponse rank(int UID, int WID, int rating){
    	Collection<Subscriber> sublist = new HashSet<Subscriber>();
    	Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist=c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		
		for(Subscriber s1 : sublist){
			if(s1.ID == UID){
				for(Wine w1 : s1.getWineList()){
					if(w1.ID==WID){
						w1.addRating(rating);
					}
					else{
						return new WineResponse(0,false,"Wine ID does not exist for the given Subscriber");
					}
				}
			}
			else{
				return new WineResponse(0, false, "Subscriber ID does not exist");
			}
		}
		
		try {
			c.subs=sublist;
			 Main.saveClubState(c);
			
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		
		return new WineResponse(1, true, "Rating added");
    }
    
    public static WineResponse add(int UID, Wine w){
    	Collection<Subscriber> sublist = new HashSet<Subscriber>();
    	Club c = new Club("VIN");
		try {
			c = Main.restoreClubState(c);
			sublist=c.subs;
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		
		for(Subscriber s1 : sublist){
			if(s1.ID == UID){
				if(s1.getPreference().equals("RW")){
					ARW arw = new ARW();
					if(arw.addWine(w)!=null){
						s1.winelist.add(w);
					}
					else{
						return new WineResponse(0, false, "Selection not of Current month");
					}
				}
				else if(s1.getPreference().equals("AW")){
					AW aw= new AW();
					if(aw.addWine(w)!=null){
						s1.winelist.add(w);
					}
					else{
						return new WineResponse(0, false, "Selection not of Current month");
					}
				}
				else if(s1.getPreference().equals("AR")){
					AR ar= new AR();
					if(ar.addWine(w)!=null){
						s1.winelist.add(w);
					}
					else{
						return new WineResponse(0, false, "Selection not of Current month");
					}
				}
			}
			else{
				return new WineResponse(0, false, "Subscriber does not exist");
			}
		}
		
		try {
			c.subs=sublist;
			 Main.saveClubState(c);
			
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}
		
		return new WineResponse(w.getID(), true, "Wine Added");
    }

}
