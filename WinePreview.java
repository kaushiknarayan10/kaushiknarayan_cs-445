import java.io.Serializable;
import java.time.Year;
import java.util.Collection;
import java.util.HashSet;


public class WinePreview implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2732810067184312483L;
	public Wine RED1(){
		WineVariety wv = WineVariety.RED;
		WineType wt = WineType.TABLE;
		String labelName = "Unico Ribera del Duero";
		String grape = "Merlot";
		String region = "Valladolid";
		String country = "Spain";
		String maker = "Vega Sicilia";
		Year year = Year.parse("1995");
		Wine w = new Wine(wv, wt,labelName, grape, region, country, maker, year);
		return w;
		
	}
	public Wine RED2(){
		WineVariety wv = WineVariety.RED;
		WineType wt = WineType.SWEET;
		String labelName = "Grand Vin Premier";
		String grape = "Bordeaux Blend Red";
		String region = "Bordeaux";
		String country = "France";
		String maker = "Margaux";
		Year year = Year.parse("1999");
		Wine w = new Wine(wv, wt,labelName, grape, region, country, maker, year);
		return w;
	}
	public Wine RED3(){
		WineVariety wv = WineVariety.RED;
		WineType wt = WineType.SWEET;
		String labelName = "Velha Tinto";
		String grape = "Touriga";
		String region = "Douro";
		String country = "Portugal";
		String maker = "Sogrape";
		Year year = Year.parse("1999");
		Wine w = new Wine(wv, wt,labelName, grape, region, country, maker, year);
		return w;
	}
	public Wine WHITE1(){
		WineVariety wv = WineVariety.WHITE;
		WineType wt = WineType.SPARKLING;
		String labelName = "Sauternes";
		String grape = "Sauvignon";
		String region = "Bordeaux";
		String country = "France";
		String maker = "Chateau d'Yquem";
		Year year = Year.parse("1996");
		Wine w = new Wine(wv, wt,labelName, grape, region, country, maker, year);
		return w;
	}
	public void displayWines(){
		Collection<Wine> wines = new HashSet<Wine>();
		wines.add(RED1());
		wines.add(RED2());
		wines.add(RED3());
		System.out.println("Wines :");
		for(Wine w : wines){
			System.out.println(w.getLabelName());
		}
	}
}
