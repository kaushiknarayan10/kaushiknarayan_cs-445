import java.io.Serializable;


public class WineResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2636508019022236742L;
	private int ID;
	private boolean status;
	private String failureDescription;
	
	public WineResponse(int id, boolean s, String f) {
		this.ID = id;
		this.status = s;
		this.failureDescription = f;
	}
}
